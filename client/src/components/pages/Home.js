import React, { Fragment } from 'react';
import Search from '../advance-search/Search';

const Home = () => {
  return (
    <Fragment>
      <Search />
    </Fragment>
  );
};

export default Home;
