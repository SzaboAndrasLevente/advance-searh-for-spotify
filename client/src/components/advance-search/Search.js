import React, { useState, useContext } from 'react';
import AdvanceSearchContext from '../../context/search/advanceSearchContext';
import AlertContext from '../../context/alert/alertContext';

const Search = () => {
  const advanceSearchContext = useContext(AdvanceSearchContext);
  const alertContext = useContext(AlertContext);

  const [genre, year, setGenre, setYear] = useState('');

  const onSubmit = e => {
    e.preventDeafautl();
    if (genre == '' || year == '') {
      alertContext.setAlert('Please enter something', 'light');
    } else {
      advanceSearchContext.searchSongs({
        genre: genre,
        year: year
      });
      setYear('');
      setGenre('');
    }
  };

  const onChange = e => {
    setYear(e.target.year);
    setGenre(e.target.genre);
  };

  return (
    <div>
      <form onSubmit={onSubmit}>
        <input type="text" name="genre" placeholder="Music Genre" />
        <input type="text" name="year" placeholder="Year" />
        <input type="submit" value="Search" />
      </form>
    </div>
  );
};

export default Search;
