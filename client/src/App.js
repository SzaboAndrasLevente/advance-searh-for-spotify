import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';
import AdvanceSearchState from './context/search/AdvanceSearchState';
import Home from './components/pages/Home';

const App = () => {
  return <AdvanceSearchState>
    <Router>
      <Switch>
        <Route exact path='/' component={Home}/>
      </Switch>
    </Router>
  </AdvanceSearchState>;
};

export default App;
