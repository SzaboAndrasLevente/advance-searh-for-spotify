import { createContext } from "react";

const advanceSearchContext = createContext();

export default advanceSearchContext;