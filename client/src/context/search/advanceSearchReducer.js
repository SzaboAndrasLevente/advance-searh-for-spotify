import {SET_LOADING, GET_SONGS} from '../types';

export default (state, action) => {
    switch(action.type) {
        case SET_LOADING:
            return {
                ...state,
                loading: true
            }
            case GET_SONGS: 
            return {
                ...state,
                songs: payload.songs,
                loading: false
            }
        default: return state;
    }
}