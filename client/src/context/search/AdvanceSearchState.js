import axios from 'axios';
import React, { useReducer } from 'react';
import AdvanceSearchContext from './advanceSearchContext';
import AdvanceSearchReducer from './advanceSearchContext';
import { SET_LOADING, GET_SONG, GET_SONGS } from '../types';

const AdvanceSearchState = props => {
  const initialState = {
    searchConditions: {},
    songs: [],
    song: {},
    loading: false
  };

  const [state, dispatch] = useReducer(AdvanceSearchReducer, initialState);

  const urlParams = new URLSearchParams(window.location.search);
  const isUserAuthorized = urlParams.has('authorized') ? true : false;

  const searchSongs = async searchConditions => {
    setLoading();

    if (isUserAuthorized) {
      const res = await axios.post('http://localhost:5000/songs', {
        searchConditions: searchConditions
      });
      dispatch({
        type: GET_SONGS,
        payload: res.data
      });
    }
  };

  // Set Loading
  const setLoading = () => dispatch({ type: SET_LOADING });

  return (
    <AdvanceSearchContext.Provider
      value={{
        loading: state.loading,
        songs: state.songs,
        song: state.song,
        searchSongs
      }}
    >
      {props.children}
    </AdvanceSearchContext.Provider>
  );
};

export default AdvanceSearchState;
