const fetch = require('node-fetch');

const getSongs = (accessToken,searhConditions)  => {
  const url = `https://api.spotify.com/v1/search?genre=${searhConditions.genre}&year:${searhConditions.}&type=traks`;

  return fetch(url, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${accessToken}`
    }
  })
    .then(res => res.json())
    .then(data => data.items)
    .catch(error => console.log(error));
};

module.exports = getRecentlyPlayed;
